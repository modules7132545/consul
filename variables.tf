variable "consul" {
  type = object({
    domain     = string
    name       = string
    datacenter = string
  })
}

variable "vault" {
  type = object({
    address = string
    token   = string
  })
}
