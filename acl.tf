locals {
  bootstrap_token   = "${vault_mount.kv.path}/data/${vault_kv_secret_v2.bootstrap_token.name}"
  replication_token = "${vault_mount.kv.path}/data/${vault_kv_secret_v2.replication_token.name}"
}

resource "random_uuid" "bootstrap_token" {}

resource "vault_kv_secret_v2" "bootstrap_token" {
  mount = vault_mount.kv.path
  name  = "bootstrap_token"
  data_json = jsonencode({
    token = lower(random_uuid.bootstrap_token.result)
  })
  lifecycle {
    ignore_changes = [data_json]
  }
}

resource "random_uuid" "replication_token" {}

resource "vault_kv_secret_v2" "replication_token" {
  mount = vault_mount.kv.path
  name  = "replication_token"
  data_json = jsonencode({
    token = lower(random_uuid.replication_token.result)
  })
  lifecycle {
    ignore_changes = [data_json]
  }
}

resource "vault_policy" "acl_policy" {
  name   = "acl-policy"
  policy = <<EOF
  path "${local.bootstrap_token}" {
  	capabilities = ["read"]
  }
  path "${local.replication_token}" {
  	capabilities = ["read", "update", "delete"]
  }
  EOF
}

resource "vault_kubernetes_auth_backend_role" "consul-server-acl-init" {
  bound_service_account_names      = ["${var.consul.name}-server-acl-init"]
  bound_service_account_namespaces = [kubernetes_namespace_v1.consul.metadata[0].name]
  token_policies                   = [vault_policy.acl_policy.name]
  backend                          = "kubernetes"
  role_name                        = "consul-server-acl-init"
  token_ttl                        = 3600
}

resource "vault_consul_secret_backend" "consul" {
  depends_on = [helm_release.consul]
  address    = "${var.consul.name}-server.consul:8500"
  path       = "${var.consul.name}-consul"
  token      = random_uuid.bootstrap_token.result
}
