terraform {
  backend "kubernetes" {
    config_path   = "~/.kube/config"
    secret_suffix = "consul"
    # config_context = "interfax-dev"
    config_context = "default"
  }
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }
    minio = {
      source  = "aminueza/minio"
      version = "2.0.1"
    }
  }
}
