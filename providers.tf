provider "helm" {
  kubernetes {
    config_path    = "~/.kube/config"
    config_context = "default"
  }
}

data "vault_generic_secret" "minio" {
  path = "minio/data"
}

locals {
  minio = data.vault_generic_secret.minio.data
}

provider "minio" {
  // required
  minio_server   = local.minio["API_HOST"]
  minio_user     = local.minio["ADMIN_USER"]
  minio_password = local.minio["ADMIN_PASSWORD"]

  // optional
  minio_region      = local.minio["REGION"]
  minio_api_version = "v4"
  minio_ssl         = true
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "default"
}

provider "vault" {
  address = var.vault.address
  token   = var.vault.token

}
