resource "vault_kubernetes_auth_backend_role" "consul-client" {
  bound_service_account_names      = ["${var.consul.name}-connect-injector", "${var.consul.name}-sync-catalog"]
  bound_service_account_namespaces = [kubernetes_namespace_v1.consul.metadata[0].name]
  token_policies                   = [vault_policy.gossip.name, vault_policy.ca-policy.name]
  backend                          = "kubernetes"
  role_name                        = "consul-client"
  token_ttl                        = 3600
}
