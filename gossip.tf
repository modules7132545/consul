resource "random_password" "consul_gossip_key" {
  length  = 32    # Длина ключа в байтах
  special = false # Без специальных символов
}

locals {
  gossip-path = "${vault_mount.kv.path}/data/${vault_kv_secret_v2.gossip.name}"
}

resource "vault_kv_secret_v2" "gossip" {
  mount = vault_mount.kv.path
  name  = "gossip"
  data_json = jsonencode({
    "key" = random_password.consul_gossip_key.result
  })
  lifecycle {
    ignore_changes = [data_json]
  }
}

resource "vault_policy" "gossip" {
  name   = "gossip"
  policy = <<EOF
    path "${local.gossip-path}" {
      capabilities = ["read"]
    }
    EOF
}
