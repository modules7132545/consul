resource "vault_kubernetes_auth_backend_role" "ca" {
  bound_service_account_names      = ["*"]
  bound_service_account_namespaces = [kubernetes_namespace_v1.consul.metadata[0].name]
  token_policies                   = ["ca"]
  backend                          = "kubernetes"
  role_name                        = "ca"
  token_ttl                        = 3600
}


locals {
  ca-cert-path = "${vault_mount.pki.path}/cert/${vault_kubernetes_auth_backend_role.ca.role_name}"
}

resource "vault_policy" "ca-policy" {
  name   = "ca"
  policy = <<EOF
	path "${local.ca-cert-path}" {
		capabilities = ["read"]
	}
	EOF
}
