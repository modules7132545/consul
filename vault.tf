resource "vault_mount" "kv" {
  path        = "consul-kv"
  type        = "kv"
  options     = { version = "2" }
  description = "consul secrets"
}
