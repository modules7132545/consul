resource "vault_mount" "pki" {
  path                  = "pki"
  type                  = "pki"
  max_lease_ttl_seconds = 87600 * 3600
}

resource "vault_pki_secret_backend_root_cert" "consul-server" {
  common_name = "${var.consul.name}.${var.consul.domain}"
  ttl         = 315360000
  type        = "internal"
  backend     = vault_mount.pki.path
}

resource "vault_pki_secret_backend_role" "consul-server" {
  allowed_domains    = ["${var.consul.datacenter}.${var.consul.domain}", "${var.consul.name}-server", "${var.consul.name}-server.${kubernetes_namespace_v1.consul.metadata[0].name}", "${var.consul.name}-server.${kubernetes_namespace_v1.consul.metadata[0].name}.svc"]
  allow_subdomains   = true
  allow_bare_domains = true
  allow_localhost    = true
  ttl                = 3600
  name               = "consul-server"
  backend            = vault_mount.pki.path
}

locals {
  server-policy      = "server-policy"
  consul-server-path = "${vault_mount.pki.path}/issue/${vault_kubernetes_auth_backend_role.consul-server.role_name}"
}

resource "vault_policy" "server-policy" {
  name   = local.server-policy
  policy = <<EOF
	path "${local.consul-server-path}" {
		capabilities = ["create", "update"]
	}
	EOF
}

resource "vault_kubernetes_auth_backend_role" "consul-server" {
  bound_service_account_names      = ["${var.consul.name}-server"]
  bound_service_account_namespaces = [kubernetes_namespace_v1.consul.metadata[0].name]
  token_policies                   = [local.server-policy, vault_policy.ca-policy.name, vault_policy.gossip.name, vault_policy.acl_policy.name]
  backend   = "kubernetes"
  role_name = "consul-server"
  token_ttl = 3600
}
