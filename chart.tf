# locals {
#   

#   
#   snapshot           = "${vault_mount.kv.path}/data/${vault_kv_secret_v2.snapshot.name}"
# }

resource "kubernetes_namespace_v1" "consul" {
  metadata {
    name = "consul"
  }
}

locals {
  template = templatefile("${path.module}/consulValues.yml", {
    #   vault = {
    #     address             = "http://vault.vault:8200"
    #     authMethodPath      = "kubernetes"
    #     rootPKIPath         = vault_mount.root_pki.path
    #     intermediatePKIPath = vault_mount.intermediate_pki.path
    #   }
    gossip = {
      path = local.gossip-path
      key  = "key"
    }
    consul = {
      name       = var.consul.name
      domain     = var.consul.domain
      datacenter = var.consul.datacenter
      #     controller = {
      #       role = vault_pki_secret_backend_role.controller_role.name
      #     }
      #     inject = {
      #       role = vault_pki_secret_backend_role.connect_inject_role.name
      #       ca   = local.controller_ca
      #       path = local.connect_inject_policy
      #     }
      server = {
        #       snapshot = {
        #         path = local.snapshot
        #         key  = "key"
        #       }
        role = vault_kubernetes_auth_backend_role.consul-server.role_name
        path = local.consul-server-path
        acl = {
          role = vault_kubernetes_auth_backend_role.consul-server-acl-init.role_name
          path = local.bootstrap_token
          key  = "token"
          replication = {
            path = local.replication_token
            key  = "token"
          }
        }
      }
      client = {
        role = vault_kubernetes_auth_backend_role.consul-client.role_name
      }
      ca = {
        role = vault_kubernetes_auth_backend_role.ca.role_name
        path = local.ca-cert-path
      }
    },

  })
}

output "template" {
  value = local.template
}

resource "helm_release" "consul" {
  name       = "consul"
  chart      = "consul"
  repository = "https://helm.releases.hashicorp.com"
  version    = "1.4.0"
  namespace  = kubernetes_namespace_v1.consul.metadata[0].name
  values     = [local.template]
  set {
    name  = "syncCatalog.enabled"
    value = true
  }
  set {
    name  = "syncCatalog.default"
    value = false
  }
  set {
    name  = "ingressGateways.enabled"
    value = true
  }
  set {
    name  = "terminatingGateways.enabled"
    value = true
  }
}
